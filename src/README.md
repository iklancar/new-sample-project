# NEW SAMPLE PROJECT #

## Start your next project in seconds!

Created | Feb. 2015
--------|-------
Author  | [Anze Klancar](https://twitter.com/iKlancar)

### What is this repository for? ###

* Starting your next project with ready to use dependencies, ready to start buiding on latest web tehnologies.

### What do I get? ###
* templating system
* sass compiling
* watch
* live-reload
* bootstrap 4
* css minify
* html-clean
* ftp-deploy

### How do I set up? ###

	$ npm install 

	$ grunt dev

### How do I work with watch? ###

	$ grunt watch

### Missing

* js minify