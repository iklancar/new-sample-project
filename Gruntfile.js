'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            options: {
                interval: 1500
            },
            sass: {
                files: ['src/scss/**/*.scss'],
                tasks: ['sass:compileDev'],
                options: {
                    livereload: true
                }
            },
            templates: {
                files: ['src/templates/**/*.hbs'],
                tasks: ['compile-handlebars'],
                options: {
                    livereload: true
                }
            },
            partials: {
                files: ['src/partials/**/*.hbs'],
                tasks: ['compile-handlebars'],
                options: {
                    livereload: true
                }
            }
        },
        clean: ["dist"],
        sass: {
            compileDev: {
                options: {
                    style: 'expanded'
                },
                files: [{
                    expand: true,
                    cwd  :'src/scss/',
                    src  :'*.scss',
                    dest :'dist/css',
                    ext  :'.min.css'
                }]
            },
            compileDist: {
                options: {
                    style: 'compressed'
                },
                files: [{
                    expand: true,
                    cwd  :'src/scss/',
                    src  :'*.scss',
                    dest :'dist/css',
                    ext  :'.min.css'
                }]
            }
        },
        'compile-handlebars': {
            default: {
                template: 'src/templates/**/*.hbs',
                templateData: 'src/data/**/*.json',
                output: 'dist/**/*.html',
                partials: 'src/partials/**/*.hbs',
            }
        },
        copy: {
            'img': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/img',
                        src: '**/*',
                        dest: 'dist/img',
                        filter: 'isFile',
                        flatten: true,
                    }
                ]
            },
            'js': {
                files: [
                    {
                        expand: true,
                        cwd: 'src/js',
                        src: '**/*',
                        dest: 'dist/js',
                        filter: 'isFile',
                    }
                ]
            },
            'bootstrap': {
                files: [
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/fonts/*'],
                        dest: 'dist/fonts',
                        flatten: true,
                        filter: 'isFile'
                    },
                    {
                        expand: true,
                        src: ['node_modules/bootstrap/dist/**/*.js'],
                        dest: 'dist/js',
                        flatten: true,
                        filter: 'isFile'
                    }
                ]
            },
            'htaccess': {
                files: [
                    {
                        expand: true,
                        src: 'src/.htaccess',
                        dest: 'dist/',
                        filter: 'isFile',
                        flatten: true
                    }
                ]
            }
        },
        // htmlclean: {
        //     deploy: {
        //         expand: true,
        //         cwd: 'dist',
        //         src: '**/*.html',
        //         dest: 'dist'
        //     }
        // }
        // imagemin: {
        //     dynamic: {
        //         files: [{
        //             expand: true,
        //             cwd: 'dist/img',
        //             src: ['**/*.{png,jpg,gif}'],
        //             dest: 'dist/'
        //         }]
        //     }
        // },
        // concat: {
        //     options: {
        //         stripBanners: true,
        //         banner: '/* <%= pkg.name %> -> <%= pkg.accName %> - v<%= pkg.version %> - ' +
        //                 '<%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %>\n' +
        //                 '<%= pkg.homepage ? "* " + pkg.homepage + "\\n" : "" %>' +
        //                 ' * Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;' +
        //                 ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %> */\n' +
        //                 '\n',
        //         },
        //         dist: {
        //             src: ['dist/test.html'],
        //             dest: 'dist/test.html',
        //     },
        // },
        // 'ftp-deploy': {
        //     'production': {
        //         auth: {
        //             host: 'ftp.xxx-xxx.si',
        //             port: 21,
        //             authKey: 'xxx-xxx'
        //         },
        //         src: 'dist',
        //         dest: '/public_html',
        //         exclusions: ['dist/.DS_Store', 'dist/Thumbs.db', 'dist/tmp'],
        //         server_sep: '/'
        //     }
        // }
    });

    // Load Grunt plugins
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.registerTask('make', ['compile-handlebars']);
    grunt.registerTask('dev', ['clean', 'sass:compileDev', 'compile-handlebars', 'copy']);
    grunt.registerTask('production', ['clean', 'sass:compileDist', 'compile-handlebars', 'copy', 'ftp-deploy:production']);

    grunt.task.registerTask('default', 'default', function(id, debug) {
        grunt.log.subhead('grunt dev');
        grunt.log.writeln('Builds all files for local development. Availible at "www.xxx-xxx.dev".');
        grunt.log.subhead('grunt production');
        grunt.log.writeln('Builds all files and ftp to live server. Availible at "www.xxx-xxx.si".');
    });
};